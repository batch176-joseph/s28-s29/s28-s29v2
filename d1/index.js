//[OBJECTIVE] Create a server-side app using Express Web Framework.

const express = require("express");

const port = 4000;

const application = express();

//[SECTION] Setup a *Middleware*
	//apply a new setting using a middleware so that the server will be able to handle/recognize and read information/data written in a JSON format.
	//json() -> allows your app to read json data.
application.use(express.json());
	//Middleware -> described as a software that provides common services and capabilities/utilities to applications outside of what's offered by the operating system.
	//API management is one of the common applications of middleware.

application.listen(port, () => 

	console.log(`Welcome to our Express API Server on port: ${port}`)

);

	application.get('/', (req, res) => {
	
			res.send(`Greetings! Welcome to course booking of Sean Joseph`);

	});


//[OBJECTIVE] Create an API collection to for our mock course booking system.

//[SECTION] Mock Database
	//create a temporary storage for the resources that we will attempt to create.
	//this is the container for our mock data.
	const courses = []; //collection of the subjects
	const users = []; //collection of the users

//[SECTION] Retrieve [GET]
	//1. Retrieve all courses inside the collection
	//SYNTAX: get(URI/path, method/callback)
	application.get('/courses', (req, res) => {
	//identify what will be returned to the client.
		//display our courses collection so the client can view our available resources.
		//using the send(), transmit the courses collection back to the client side.
		//we need to identify the response/feedback of the server once the client attempts to send a request to this endpoint.
		res.send(courses);
	});

	//2. Retrieve all users 
	application.get('/users', (mensahe, responde) => {
		//describe what will be transmitted back to the client.
		responde.send(users);
	});


//[SECTION] Create [POST]
	//1. Create course
	//we will now create a request that will allow us to insert an entry/resource in our collection.
	//post() -> the request that will be created using the post() will expect a POST method tpye of request from the client.
	//SYNTAX: server.post()

	//setup the request to identify what data will be transmitted in this new route.
	application.post('/course', (req, res) => {
	//identify how the data will be processed by the server once the data has successfully been transmitted over the network.
	//insert a temporary response to check if the request setup in postman is correct
	// res.send('This is the correct route to create a new course');
	//this request should also be able to transmit data over the network.
	//check if the data will be transmitted over the API.
	console.log(req.body); //we were able to determine that the server INDEED was able to receive data from the client, HOWEVER it resulted to an "undefuned" data because at this point our server-side app is NOT able to recognize data written in JSON format.

	//process the information from the client's request and identify a point where the transmission will be terminated.
	let newCourse = req.body;
	let courseName = req.body.name;
	let courseInt = req.body.instructor;
	let courseCost = req.body.price;

	//we will create a logic that will validate the data making sure that we got the necessary information needed before we save the document inside the collection.
	//Create a control structure that will validate the data inserted by the client to make sure that they are complete.
	//we are going to insert a condition that will allow us to specify the parameters needed before inserting the data inside the collection.
	if (courseName !== '' && courseInt !== '' && courseCost !== '' ) {
		//this block of code will run if the condition is MET.
		//insert the data inside our collection
		courses.push(newCourse);
		res.send(`New ${courseName} Course has been added in our collection`);
	} else {
		//this block of code will run if the condition is NOT MET.
		//let's decide/identify a response if the client would fail to meet all the parameters needed.
		res.send('Make sure that Course Name, Instructor, and Price is complete');
	}

	});

	//2. Create New User
		//this will expect a 'POST' method type of request.
	application.post('/user', (req, res) => {
		//create a temporary response just to make sure that the setup of the request is correct.
		// res.send('This is the correct route to insert a new user');
		//identify the information/data that will describe a single user in our collection.

		//create a checker to make sure that the data from the request will be transmitted over the network.
		console.log(req.body);
		let newUser = req.body;
		let userFname = req.body.firstName;
		let userLname = req.body.lastName;
		let userStats = req.body.status;
		let userMobil = req.body.mobileNumber;

		//let's validate the data first before allowing the document inside our collection.

		if (
			userFname !== '' &&
			userLname !== '' &&
			userStats !== '' &&
			userMobil !== ''
		) {
			//this block of code will run if the condition is MET.
			//save the user inside the users collection.
			users.push(newUser);
			res.send(`New user ${userFname} has been created`)
		} else {
			//this block of code will run if the condition is NOT MET.
			res.send('Make sure First Name, Last Name, Status, and Mobile Number are all complete')
		}

	});


//[SECTION] Update [PUT]
	//1. Update Course Price
		//if were going to put(), this will expect a 'PUT' http request
		//SYNTAX: put(URI/path, method/callback)
	application.put('/change-price', (req, res) => {
		//describe how the resource will be targeted together with the updates that will be made.
			// res.send('This is to change course price'); //Temporary response
		//name -> reference target
		//identify if the collection is empty or not.
		let message;
		// console.log(req.body);
		let cTarget = req.body.name;
		let cUpdate = req.body.price;

		if (courses.length !== 0) {
			//NOT EMPTY
			//start a query to conduct a search to find the desired document inside the collection.
			for (let index = 0; index < courses.length; index++) {
				//per iteration of the loop according to the number of elements inside the collection we will add a value of 1.
				//check if the data from the request has been transmitted.
				//find a *match* in the collection for the target.
				if (cTarget === courses[index].name) {
					//Match Found

					//update the desired change on the course price.
					//call out the variable that holds the changes we desire.
					courses[index].price = cUpdate;
					//this is to repackage a new value to the price property of the current element according to the current iteration of the loop.

					message = `${cTarget} Found and Updated to ${cUpdate}`;
					//terminate the loop if it did not reach the end of the array.
					break;

				} else {
					//No Match Found
					message = `No Match Found for ${cTarget}`;

				}

			};
			

		} else {
			//EMPTY
			message = 'no courses found';

		}
		res.send(message);

	});

	//2. Update User Status
	application.put('/change-status', (req, res) => {
		//information will be processed here
		// console.log(req.body);
		let uTarget = req.body.lastName;
		let uUpdate = req.body.status;
		let message;

		//check if the collection is populated or not.
		if (users.length !== 0) {
			//not empty
			//conduct a search to find a match for the targeted user.
			for (let indexNum = 0; indexNum < users.length; indexNum++) {
				//this loop will run to browse through each element inside the array structure.
				//create a logic that will find a match for the targeted document.
				let lNameOfElement = users[indexNum].lastName;
				
				if (uTarget === lNameOfElement) {
					//match
					let currentUserStatus = users[indexNum].status;

					//override the current value of the user's status
					message = `Match Found for ${uTarget} and status has been set from ${currentUserStatus} to ${uUpdate}`;
					users[indexNum].status = uUpdate;
					/*currentUserStatus = users[indexNum].status;
					currentUserStatus = uUpdate;*/
					//terminate the loop once a match is found.
					break;


				} else {
					//not match
					message = `No Match Found for ${uTarget}`;
				}



			}
			// message = 'users found';

		} else {
			//empty
			message = 'users not found';

		}


		res.send(message); //temporary response


	});


//[SECTION] Destroy [DEL]
	//1. Delete a course from the collection
		//Upon executing a request using delete(), this request will be expecting a 'DEL' method type.
		//SYNTAX: serverName.delete(URI/path, callback/method)
	application.delete('/course', (req, res) => {
		//create a temporary response just to make sure if the client setup is correct
		// res.send('This is correct route for DELETING a resource');

		//Create a variable to store the message to be sent back to the client via Postman.
		let message;
		//checker if data is successfully transmitted via the api
		// console.log(req.body);
		let targetReference = req.body.name;
		//before deleting any resource you have to make sure you are destroying the correct data.
		//select a reference target that we will use to determine which course we want to delete.
		//we will select the name of the course as reference target.
	//Create a logic that will check iof the collectioon/database is not empty.
		//assess the contents of the courses collection to properly create a conditional statement
	//Create a logic that will perform a query to find the desired resource to be deleted using the proper reference target.
	if (courses.length !== 0) {
		//create a for loop that brose through the elements inside our courses array
		for(let index = 0; index < courses.length; index++){
			//in each iteration of our loop we will try to find a match for the courseName in the client's request with the current items or current documents or current resources save inside the collection.
			if (targetReference === courses[index].name) {
			//destroy the resource that was Found during the query.
			//in order to remove an element inside the array we are going to use a mutator method of array structures.
			//SYNTAX: arrayName.splice(start, delete/removeCount)
				//courses[index] -> is used to indicate the start of the index number to identify the element inside the array.
				//The number 1 -> defines the number of elements to be removed inside the array.
			courses.splice(courses[index], 1)
			message = `A Match for ${targetReference} has been found and deleted from the resources`;
			//once a resource is found break the loop
			break;
			} else {
				message = 'No Match found';
			}

		}
	} else {
		message = 'EMPTY collection';
	};

	//send/transmit the message back to the client.
	res.send(message); //test if it will work


	});

	//2.Delete user
	application.delete('/user', (req, res) => {
		//temporary response to check the postman req setup
		// res.send('This is the correct route for delete user');
		//select a proper identifier in order to reference the user that you wish to delete.
		//we will use the last Name to reference the proper user.

		//select a container for the message that will be sent back to the client.
		let liham;
		let queryRef = req.body.lastName;
		console.log(queryRef);

		//create a logic that will check if the database is not empty.
		if (users.length !== 0) {
			//not empty
			liham = 'Users Collection is Not empty';

			//create a search query to find the user you want to delete.
			for (let indexCount = 0; indexCount < users.length; indexCount++){
				//the purpose of this query is to find a Match for our reference target inside our collection.
				if (queryRef === users[indexCount].lastName) {
					//Match was Found
					//delete a user from the collection
					//startIndex -> which element will the extraction begin
					//removeCount -> the number of elements to be deleted beginning from the start index
					users.splice(indexCount, 1);
					liham = `Match Found for ${queryRef} and deleted from the collection`;
					//terminate the progress of the loop once a match is found.
					break;
				} else {
					//No Match was Found
					liham = 'Found No Matches!';
				}
			}
		} else {
			//empty collection
			liham = 'Users Collection is EMPTY';

		}

		res.send(liham);
	});


//[SECTION] Export our user API collection.

